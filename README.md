## Scripts

- yarn start - Runs the project in development mode. You can view your application at http://localhost:3000
- yarn test - Runs the test watcher (Jest) in an interactive mode.
- yarn test:coverage - Runs the test watcher (Jest) in coverage mode.

##Tools and packages

- [jaredpalmer/razzle](https://github.com/jaredpalmer/razzle) - Razzle Boilerplate
- [smooth-code/loadable-components](https://github.com/smooth-code/loadable-components) - React code-splitting with server side support
- [prettier/prettier](https://github.com/prettier/prettier) - Code formatter. Using eslint plugin
