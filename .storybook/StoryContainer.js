import React from 'react';
import PropTypes from 'prop-types';
import { MemoryRouter } from 'react-router';
import { AppContainer, hot } from 'react-hot-loader';

import './story.css';


const StoryContainer = ({ children }) => {
  return (
    <AppContainer>
      <MemoryRouter>{children}</MemoryRouter>
    </AppContainer>
  );
};

StoryContainer.propTypes = {
  children: PropTypes.any,
};

export default hot(module)(StoryContainer);
