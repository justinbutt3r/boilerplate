// const createCompiler = require('@storybook/addon-docs/mdx-compiler-plugin');

const postCssOptions = {
  ident: 'postcss', // https://webpack.js.org/guides/migrating/#complex-options
  sourceMap: false,
  plugins: [
    require('autoprefixer')({
      browsers: [
        '>1%',
        'last 4 versions',
        'Firefox ESR',
        'not ie < 9', // React doesn't support IE8 anyway
      ],
    }),
  ],
};

const path = require('path');

// Export a function. Accept the base config as the only param.
module.exports = async ({ config, mode }) => {
  // `mode` has a value of 'DEVELOPMENT' or 'PRODUCTION'
  // You can change the configuration based on that.
  // 'PRODUCTION' is used when building the static version of storybook.

  // Make whatever fine-grained changes you need
  config.module.rules.push({
    test: /\.scss$/,
    use: [
      'style-loader',
      'css-loader',
      'sass-loader',
      { loader: 'postcss-loader', options: postCssOptions },
    ],
    include: path.resolve(__dirname, '../'),
  });

  // config.module.rules.push({
  //   test: /\.(stories|story)\.mdx$/,
  //   use: [
  //     // {
  //     //   loader: 'babel-loader',
  //     //   // may or may not need this line depending on your app's setup
  //     //   options: {
  //     //     plugins: ['@babel/plugin-transform-react-jsx'],
  //     //   },
  //     // },
  //     {
  //       loader: '@mdx-js/loader',
  //       options: {
  //         compilers: [createCompiler({})],
  //       },
  //     },
  //   ],
  // });
  //
  // config.module.rules.push({
  //   test: /\.(stories|story)\.[tj]sx?$/,
  //   loader: require.resolve('@storybook/source-loader'),
  //   exclude: [/node_modules/],
  //   enforce: 'pre',
  // });

  // Return the altered config
  return config;
};
