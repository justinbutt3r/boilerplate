import React from 'react';
import { addParameters, configure, addDecorator } from '@storybook/react';
import { themes } from '@storybook/theming';
import StoryContainer from './StoryContainer';

addDecorator(story => <StoryContainer>{story()}</StoryContainer>);

addParameters({
  options: {
    panelPosition: 'bottom',
    theme: themes.light,
    storySort: (a, b) =>
      a[1].id === 'welcome--page' || b[1].id === 'welcome--page'
        ? -1
        : a[1].kind === b[1].kind
        ? 0
        : a[1].id.localeCompare(b[1].id, { numeric: true }),
  },
  backgrounds: [
    { name: 'white', value: '#fff', default: true },
    { name: 'black', value: '#000' },
    { name: 'twitter', value: '#00aced' },
    { name: 'facebook', value: '#3b5998' },
  ],
});

configure(
  require.context('../src/stories', true, /\.stories\.(js|mdx)$/),
  module
);
