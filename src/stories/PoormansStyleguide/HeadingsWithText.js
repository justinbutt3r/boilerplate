import React from 'react';
import './styles.css';

function HeadingsWithText() {
  return (
    <div>
      <h1 className="title" id="headings-with-text">Headings with Text</h1>

      <h1>Heading 1</h1>
      <p>
        Lorem ipsum dolor sit amet, adipiscing elit. Nullam dignissim convallis
        est. Quisque aliquam. Donec faucibus. Nunc iaculis suscipit dui. Nam sit
        amet sem. Aliquam libero nisi, imperdiet at, tincidunt nec, gravida
        vehicula, nisl.
      </p>
      <h2>Heading 2</h2>
      <p>
        Lorem ipsum dolor sit amet, adipiscing elit. Nullam dignissim convallis
        est. Quisque aliquam. Donec faucibus. Nunc iaculis suscipit dui. Nam sit
        amet sem. Aliquam libero nisi, imperdiet at, tincidunt nec, gravida
        vehicula, nisl.
      </p>
      <h3>Heading 3</h3>
      <p>
        Lorem ipsum dolor sit amet, adipiscing elit. Nullam dignissim convallis
        est. Quisque aliquam. Donec faucibus. Nunc iaculis suscipit dui. Nam sit
        amet sem. Aliquam libero nisi, imperdiet at, tincidunt nec, gravida
        vehicula, nisl.
      </p>
      <h4>Heading 4</h4>
      <p>
        Lorem ipsum dolor sit amet, adipiscing elit. Nullam dignissim convallis
        est. Quisque aliquam. Donec faucibus. Nunc iaculis suscipit dui. Nam sit
        amet sem. Aliquam libero nisi, imperdiet at, tincidunt nec, gravida
        vehicula, nisl.
      </p>
      <h5>Heading 5</h5>
      <p>
        Lorem ipsum dolor sit amet, adipiscing elit. Nullam dignissim convallis
        est. Quisque aliquam. Donec faucibus. Nunc iaculis suscipit dui. Nam sit
        amet sem. Aliquam libero nisi, imperdiet at, tincidunt nec, gravida
        vehicula, nisl.
      </p>
      <h6>Heading 6</h6>
      <p>
        Lorem ipsum dolor sit amet, adipiscing elit. Nullam dignissim convallis
        est. Quisque aliquam. Donec faucibus. Nunc iaculis suscipit dui. Nam sit
        amet sem. Aliquam libero nisi, imperdiet at, tincidunt nec, gravida
        vehicula, nisl.
      </p>
    </div>
  );
}

export default HeadingsWithText;
