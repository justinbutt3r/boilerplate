import React from 'react';

import { storiesOf } from '@storybook/react';

import Headings from './Headings';
import HeadingsWithText from './HeadingsWithText';
import BlockElements from './BlockElements';
import TextElements from './TextElements';
import MonospacedPreformatted from './MonospacedPreformatted';
import ListTypes from './ListTypes';
import Tables from './Tables';
import Media from './Media';
import FormElements from './FormElements';

storiesOf("Poorman's Styleguide", module)
  .add('Headings', () => <Headings />)
  .add('Headings with Text', () => <HeadingsWithText />)
  .add('Block Elements', () => <BlockElements />)
  .add('Text Elements', () => <TextElements />)
  .add('Monospaced / Preformatted', () => <MonospacedPreformatted />)
  .add('List Types', () => <ListTypes />)
  .add('Tables', () => <Tables />)
  .add('Media', () => <Media />)
  .add('Form Elements', () => <FormElements />);
