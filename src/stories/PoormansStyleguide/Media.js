import React from 'react';
import './styles.css';

function Media() {
  return (
    <div>
      <h1 className="title"  id="media-elements">Media</h1>

      <h2>The Audio Element:</h2>
      <audio controls>
        <source
          src="https://simpl.info/track/audio/audio/animalSounds.ogv"
          type="audio/ogg"
        />
        <source
          src="https://simpl.info/track/audio/audio/animalSounds.mp3"
          type="audio/mpeg"
        />
        <track
          label="English subtitles"
          src="/video/developerStories-subtitles-en.vtt"
          kind="subtitles"
          srcLang="en"
          default
        />
        <p>Your browser does not support the audio element.</p>
      </audio>

      {/* <source src="audio/animalSounds.mp3" /> */}
      {/* <source src="audio/animalSounds.ogv" /> */}

      <h2>The Video Element:</h2>
      <video width="320" height="240" controls>
        <source
          src="https://simpl.info/track/video/developerStories-en.webm"
          type="video/webm;codecs=&quot;vp8, vorbis&quot;"
        />
        <source
          src="https://simpl.info/track/video/developerStories-en.mp4"
          type="video/mp4;codecs=&quot;avc1.42E01E, mp4a.40.2&quot;"
        />
        <track
          src="/video/developerStories-subtitles-en.vtt"
          label="English subtitles"
          kind="subtitles"
          srcLang="en"
          default
        />
        <p>Your browser does not support the video tag.</p>
      </video>

      <h2>Embedded content:</h2>
      <p>YouTube video (iframe):</p>
      <iframe
        title="YouTube video (iframe)"
        width="560"
        height="315"
        src="https://www.youtube.com/embed/l4f9QF0SGuQ"
        frameBorder="0"
        allow="autoplay; encrypted-media"
        allowFullScreen
      />
    </div>
  );
}

export default Media;
