import React from 'react';
import './styles.css';

function MonospacedPreformatted() {
  return (
    <div>
      <h1 className="title"  id="monospace">Monospace / Preformatted</h1>
      <p>
        Code block wrapped in {`"pre"`} and {`"code"`} tags
      </p>
      <pre>
        <code>
          {`// Loop through Divs using Javascript.
var divs = document.querySelectorAll('div'), i;
                
for (i = 0; i < divs.length; ++i) {
    divs[i].style.color = "green";
}`}
        </code>
      </pre>
      <p>Monospace Text wrapped in {`"pre"`} tags</p>
      <pre>
        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nullam
        dignissim convallis est. Quisque aliquam. Donec faucibus. Nunc iaculis
        suscipit dui. Nam sit amet sem. Aliquam libero nisi, imperdiet at,
        tincidunt nec, gravida vehicula, nisl.
      </pre>

      <hr />
    </div>
  );
}

export default MonospacedPreformatted;
